import { from, reduce } from 'rxjs';
import { filterNullish } from './filter-nullish-operator';

const inputValues = [
  null,
  undefined,
  'string290',
  '',
  false,
  true,
  1,
  -1,
  0,
  Infinity,
  -Infinity,
  [],
  [
    'e1',
    23,
  ],
  {},
  {
    'prop1': false,
    'prop2': 90,
  },
  NaN,
];

const expectedResult = [
  'string290',
  '',
  false,
  true,
  1,
  -1,
  0,
  Infinity,
  -Infinity,
  [],
  [
    'e1',
    23,
  ],
  {},
  {
    'prop1': false,
    'prop2': 90,
  },
  NaN,
];

jest.useFakeTimers();

describe('Test filterNullish operator', () => {
  it('should filter nullish values', () => {
    from(inputValues).pipe(
      filterNullish(),
      reduce((acc, val) => {
        acc.push(val);

        return acc;
      }, [] as any[]),
    ).subscribe(result => {
      expect(result).toEqual(expectedResult);
    })

    jest.advanceTimersToNextTimer();
  });
});

import { filter, OperatorFunction } from 'rxjs';
import { isNonNullish, NonNullish } from './nullable';

export function filterNullish<T>(): OperatorFunction<T, NonNullish<T>> {
  return filter((value: T): value is NonNullish<T> => isNonNullish(value));
}

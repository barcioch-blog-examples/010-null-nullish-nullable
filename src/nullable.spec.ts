import { isNonNullish, isNullish } from './nullable';

const isNullishDataset: { key: string; value: any; expectedResult: boolean }[] = [
  { key: 'null', value: null, expectedResult: true },
  { key: 'undefined', value: undefined, expectedResult: true },
  { key: 'string290', value: 'string290', expectedResult: false },
  { key: '""', value: '', expectedResult: false },
  { key: 'false', value: false, expectedResult: false },
  { key: 'true', value: true, expectedResult: false },
  { key: '1', value: 1, expectedResult: false },
  { key: '-1', value: -1, expectedResult: false },
  { key: '0', value: 0, expectedResult: false },
  { key: 'Infinity', value: Infinity, expectedResult: false },
  { key: '-Infinity', value: -Infinity, expectedResult: false },
  { key: '[]', value: [], expectedResult: false },
  { key: '["e1", 23]', value: ['e1', 23], expectedResult: false },
  { key: '{}', value: {}, expectedResult: false },
  { key: '{prop1: false, prop2: 90}', value: { prop1: false, prop2: 90 }, expectedResult: false },
  {
    key: '() => {}', value: () => {
    }, expectedResult: false
  },
  { key: 'NaN', value: NaN, expectedResult: false },
  { key: 'new Error()', value: new Error(), expectedResult: false },
];


describe('Test isNullish', () => {
  it.each(isNullishDataset)('value: $key', ({ value, expectedResult }) => {
    expect(isNullish(value)).toEqual(expectedResult);
  });
});

const isNonNullishDataset: { key: string; value: any; expectedResult: boolean }[] = [
  { key: 'null', value: null, expectedResult: false },
  { key: 'undefined', value: undefined, expectedResult: false },
  { key: 'string290', value: 'string290', expectedResult: true },
  { key: '""', value: '', expectedResult: true },
  { key: 'false', value: false, expectedResult: true },
  { key: 'true', value: true, expectedResult: true },
  { key: '1', value: 1, expectedResult: true },
  { key: '-1', value: -1, expectedResult: true },
  { key: '0', value: 0, expectedResult: true },
  { key: 'Infinity', value: Infinity, expectedResult: true },
  { key: '-Infinity', value: -Infinity, expectedResult: true },
  { key: '[]', value: [], expectedResult: true },
  { key: '["e1", 23]', value: ['e1', 23], expectedResult: true },
  { key: '{}', value: {}, expectedResult: true },
  { key: '{prop1: false, prop2: 90}', value: { prop1: false, prop2: 90 }, expectedResult: true },
  {
    key: '() => {}', value: () => {
    }, expectedResult: true
  },
  { key: 'NaN', value: NaN, expectedResult: true },
  { key: 'new Error()', value: new Error(), expectedResult: true },
];

describe('Test isNonNullish', () => {
  it.each(isNonNullishDataset)('value: $key', ({ value, expectedResult }) => {
    expect(isNonNullish(value)).toEqual(expectedResult);
  });
});

export type Nullable<T> = T | null | undefined;
export type Nullish = null | undefined;
export type NonNullish<T> = Exclude<T, null | undefined>;

export const isNullish = (value: unknown): value is Nullish => value === null || value === undefined;
export const isNonNullish = (value: unknown): value is NonNullish<unknown> => value !== null && value !== undefined;
